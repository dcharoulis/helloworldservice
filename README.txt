
                         HelloWorldService

  What is it?
  -----------

  A short description of what this project is about

  The Latest Version
  ------------------

  That could be a link to a Confluence that referst to the last updates / changes in project requirements

  Documentation
  -------------

  Can be a link to a Confluence page or a path inside the repository refering to the documentation of the project 

  Installation
  ------------

  Can be a link to a Confluence page or a path inside the repository refering to the documentation of the project

  Licensing
  ---------

  Something like a reference to licence issues or like  "Please see the file called LICENSE."


  Contacts
  --------

     o Project Manager

     o Team lead

     o Responsible for deployments/database/Jenkins job configuration

     o Link to confluence page of the project

